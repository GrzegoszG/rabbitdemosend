﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ;
using Rabbit;
using RabbitMQ.Client;
using RabbitMQ.Util;

namespace Rabbit
{
    class Program
    {

        public static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: "test-exchange", type: "direct", durable: true);

                var message = "Hello world";
                while (!string.IsNullOrEmpty(message))
                {
                    message = Console.ReadLine();

                    var body = Encoding.UTF8.GetBytes(message);
                    channel.BasicPublish(exchange: "test-exchange",
                                         routingKey: "message",
                                         basicProperties: null,
                                         body: body);
                    Console.WriteLine("Sent : {0}", message);
                }
            }

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }

        
    }
}
